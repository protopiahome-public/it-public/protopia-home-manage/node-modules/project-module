// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'project';

module.exports = {

  Mutation: {
    changeProject: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.input.author_id) {
        args.input.author_id = new ObjectId(args.input.author_id);
      }
      if (args.input.roles_ids) {
        args.input.roles_ids = args.input.roles_ids.map((e) => new ObjectId(e));
      }

      if (args._id) {
        return await query(collectionItemActor, { type: 'project', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'project', input: args.input }, global.actor_timeout);
    },
    changeStage: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'stage', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'stage', input: args.input }, global.actor_timeout);
    },
    addStageToProject: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, {
        type: 'project',
        search: { _id: args._id },
        input:
                    {
                      $addToSet: { member_ids: new ObjectId(args.stage_id) },
                    },

      }, global.actor_timeout);
    },
  },

  Query: {

    // Для корректной работы добавить полнотекстовый индекс db.project.createIndex( { name: "text" } )
    searchProjectByTitle: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, {
        type: 'project',
        search: {
          $text: { $search: `"${args.title}"` },
        },
      }, global.actor_timeout);
    },

    getProject: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'project', search: { _id: args.id } }, global.actor_timeout);
    },

    getProjects: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'project' }, global.actor_timeout);
    },

    getStages: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'stage' }, global.actor_timeout);
    },
    getStage: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'stage', search: { _id: args.id } }, global.actor_timeout);
    },
  },
  Project: {
    author: async (obj, args, ctx, info) => {
      if (obj.author_id) {
        return (await ctx.db.user.find({ _id: new ObjectId(obj.author_id) }))[0];
      }
    },
  },
};
